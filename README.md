## Spec

Removed from [Netshoes spec](https://gist.github.com/tiagoMissiato/b1d0f6826b796edaa0b5f55d1d9e8d49) wiith some alterations:

The app need to be written in Java, Kotlin or both

App Description:
[x] The app must list all public gist with pagination (the number of gists per page is up to you)
[x] Show gist details in another screen (Fragment or Activity), with the following infos: owner name with avatar, gist title and language
[x] Display raw file content in another screen
[x] Must have an option to save the gist as favorite Gist (device storage)
[x] Bottom Navigation with: Home, Favorites, About App
[ ] About App has logout button on it

Each gist must show:
[x] Author Name 
[x] Avatar
[x] Gist title
[x] Language

Technical requirement
[ ] 3rd part libs are allowed, but with caution
[x] More than one flavor, must have some difference between them (main colors, app icon, etc...)
[x] Use one of the common App Development Architecture (MVP is a plus)
[x] Internationalization (en_US, pt_BR, es_AR)
[x] Error Handling (Network, etc)
[x] Use a Custom View
[ ] Unit test
[ ] Animations & Transitions (optional)
[x] RxAndroid (optional)
[ ] Instrumented test (optional)

## Inspirations

- [Android Architecture(MVP with RxJava)](https://github.com/googlesamples/android-architecture/tree/todo-mvp-rxjava) - [Google Samples](https://github.com/googlesamples)
- [Ultimate Guide to Bottom Navigation on Android](https://android.jlelse.eu/ultimate-guide-to-bottom-navigation-on-android-75e4efb8105f) - [Suleiman Ali Shakir](https://android.jlelse.eu/@Suleiman19)

## Reference

- Public gist
  - https://developer.github.com/v3/gists/#list-all-public-gists
  - /gists/public

- Gist Detail
  - https://developer.github.com/v3/gists/#get-a-single-gist
  - /gists/:id

## License

```
The MIT License (MIT)

Copyright (c) 2017 Gabryel Monteiro

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.