package br.com.gabryel.netgists

import android.content.Context
import br.com.gabryel.netgists.about.AboutContract
import br.com.gabryel.netgists.about.AboutFragment
import br.com.gabryel.netgists.about.AboutPresenter
import br.com.gabryel.netgists.data.source.GistDataSource
import br.com.gabryel.netgists.data.source.GistRepository
import br.com.gabryel.netgists.data.source.local.Models
import br.com.gabryel.netgists.data.source.local.RequeryRepository
import br.com.gabryel.netgists.data.source.remote.GitHubDataSource
import br.com.gabryel.netgists.data.source.remote.MixedGistRepository
import br.com.gabryel.netgists.details.file.FileContract
import br.com.gabryel.netgists.details.file.FilePresenter
import br.com.gabryel.netgists.gist.*
import br.com.gabryel.netgists.service.GitHubGistService
import com.github.salomonbrys.kodein.*
import io.reactivex.schedulers.Schedulers
import io.requery.Persistable
import io.requery.android.sqlite.DatabaseSource
import io.requery.reactivex.KotlinReactiveEntityStore
import io.requery.sql.KotlinEntityDataStore
import io.requery.sql.TableCreationMode
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class Injection(private val context: Context) {

    enum class InjectionType { LOCAL, PRIMARILY_REMOTE }

    val graph = Kodein.lazy {

        bind<Context>() with instance(context)

        bind<AboutContract.Presenter>() with factory { view: AboutFragment ->
            AboutPresenter(view = view)
        }

        bind<FileContract.Presenter>(InjectionType.LOCAL) with factory {
            pair: Pair<Pair<String, String>, FileFragment> ->
            FilePresenter(gistDS = instance(InjectionType.LOCAL),
                    fileType = pair.first.first,
                    fileUrl = pair.first.second,
                    view = pair.second)
        }

        bind<FileContract.Presenter>(InjectionType.PRIMARILY_REMOTE) with factory {
            pair: Pair<Pair<String, String>, FileFragment> ->
            FilePresenter(gistDS = instance(InjectionType.PRIMARILY_REMOTE),
                    fileType = pair.first.first,
                    fileUrl = pair.first.second,
                    view = pair.second)
        }

        bind<DetailsContract.Presenter>(InjectionType.PRIMARILY_REMOTE) with factory {
            pair: Pair<String, DetailsFragment> ->
            DetailsPresenter(gistDS = instance(InjectionType.PRIMARILY_REMOTE),
                    gistId = pair.first,
                    view = pair.second)
        }

        bind<DetailsContract.Presenter>(InjectionType.LOCAL) with factory {
            pair: Pair<String, DetailsFragment> ->
            DetailsPresenter(gistDS = instance(InjectionType.LOCAL),
                    gistId = pair.first,
                    view = pair.second)
        }

        bind<GistsContract.Presenter>(InjectionType.PRIMARILY_REMOTE) with factory {
            view: GistsFragment ->
            GistsPresenter(gistDS = instance(InjectionType.PRIMARILY_REMOTE), view = view)
        }

        bind<GistsContract.Presenter>(InjectionType.LOCAL) with factory {
            view: GistsFragment ->
            GistsPresenter(gistDS = instance(InjectionType.LOCAL), view = view)
        }

        bind<GistRepository>(InjectionType.PRIMARILY_REMOTE) with singleton {
            MixedGistRepository(
                    localGistRepository = instance(InjectionType.LOCAL),
                    remoteGistDataSource = instance(InjectionType.PRIMARILY_REMOTE)
            )
        }

        bind<GistDataSource>(InjectionType.PRIMARILY_REMOTE) with singleton {
            GitHubDataSource(gistService = instance())
        }

        bind<GistRepository>(InjectionType.LOCAL) with singleton {
            RequeryRepository(entityStore = instance())
        }

        bind<GistDataSource>(InjectionType.LOCAL) with singleton {
            instance<GistRepository>(InjectionType.LOCAL)
        }

        bind<GitHubGistService>() with singleton {
            kodein.instance<Retrofit>().create(GitHubGistService::class.java)
        }

        bind<Retrofit>() with singleton {
            Retrofit.Builder().run {
                baseUrl("https://api.github.com/")
                addConverterFactory(GsonConverterFactory.create())
                addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            }.build()
        }

        bind<KotlinReactiveEntityStore<Persistable>>() with singleton {
            val source = DatabaseSource(context, Models.DEFAULT, 1)
            source.setTableCreationMode(TableCreationMode.CREATE_NOT_EXISTS)
            KotlinReactiveEntityStore<Persistable>(KotlinEntityDataStore(source.configuration))
        }
    }
}