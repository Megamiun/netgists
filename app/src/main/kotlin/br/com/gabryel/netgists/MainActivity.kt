package br.com.gabryel.netgists

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v7.app.AppCompatActivity
import br.com.gabryel.netgists.about.AboutFragment
import br.com.gabryel.netgists.gist.GistsFragment
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        content.adapter = NavAdapter(supportFragmentManager)

        val mapping = mapOf(R.id.navigation_home to 0,
                R.id.navigation_fave to 1,
                R.id.navigation_about to 2)

        navigation.setOnNavigationItemSelectedListener { item ->
            val position = mapping[item.itemId]?: return@setOnNavigationItemSelectedListener false

            content.currentItem = position
            return@setOnNavigationItemSelectedListener true
        }
    }

    inner class NavAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

        override fun getCount() = 3

        override fun getItem(position: Int): Fragment? {
            return when (position) {
                0 -> GistsFragment.newInstance(false)
                1 -> GistsFragment.newInstance(true)
                2 -> AboutFragment.newInstance()
                else -> null
            }
        }
    }
}