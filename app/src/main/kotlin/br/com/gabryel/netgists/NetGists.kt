package br.com.gabryel.netgists

import android.app.Application
import com.github.salomonbrys.kodein.KodeinAware
import io.github.kbiakov.codeview.classifier.CodeProcessor

class NetGists: Application(), KodeinAware {
    override val kodein by Injection(this).graph

    override fun onCreate() {
        super.onCreate()
        CodeProcessor.init(this)
    }
}