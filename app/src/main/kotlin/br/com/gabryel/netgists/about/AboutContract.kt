package br.com.gabryel.netgists.about

import br.com.gabryel.netgists.mvp.base.BasePresenter
import br.com.gabryel.netgists.mvp.base.BaseView

/**
 * Contract specifying the links between the About View and Presenter
 *
 * Created by gabryel on 20/01/18.
 */
class AboutContract {
    interface View : BaseView<Presenter>

    interface Presenter : BasePresenter
}