package br.com.gabryel.netgists.about

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.gabryel.netgists.R
import com.github.salomonbrys.kodein.LazyKodein
import com.github.salomonbrys.kodein.android.appKodein
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.with

class AboutFragment : Fragment(), AboutContract.View {

    override var myView: View? = null
        get() = view

    override lateinit var presenter: AboutContract.Presenter

    private val kodein = LazyKodein(appKodein)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        presenter = kodein().with(this).instance()
    }


    override fun onResume() {
        super.onResume()
        presenter.subscribe()
    }

    override fun onPause() {
        super.onPause()
        presenter.unsubscribe()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_about, container, false)

        // TODO Setup view

        return root
    }

    companion object {
        @JvmStatic
        fun newInstance() = AboutFragment()
    }
}