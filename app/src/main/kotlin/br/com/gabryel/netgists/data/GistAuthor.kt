package br.com.gabryel.netgists.data

/**
 * Immutable representation of a GistContent
 *
 * Created by gabryel on 20/01/18.
 */
data class GistAuthor(val id: Int, val login: String, val avatarUrl: String)