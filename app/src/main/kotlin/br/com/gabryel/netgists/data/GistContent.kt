package br.com.gabryel.netgists.data

/**
 * Immutable representation of a GistContent
 *
 * Created by gabryel on 20/01/18.
 */
data class GistContent(val id: String, val files: List<GistFile>, val owner: GistAuthor?)