package br.com.gabryel.netgists.data

/**
 * Immutable representation of a GistFile
 *
 * Created by gabryel on 20/01/18.
 */
data class GistFile(val filename: String,
                    val language: String?,
                    val rawUrl: String,
                    val fileType: String)