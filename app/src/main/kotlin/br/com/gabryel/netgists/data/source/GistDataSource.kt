package br.com.gabryel.netgists.data.source

import android.graphics.Bitmap
import br.com.gabryel.netgists.data.GistAuthor
import br.com.gabryel.netgists.data.GistContent
import io.reactivex.Flowable
import io.reactivex.Maybe

/**
 * Data Source for retrieval of Gists gistLocalStore
 *
 * Created by gabryel on 21/01/18.
 */
interface GistDataSource {
    fun getGists(pageNo: Int, pageSize: Int = 30): Flowable<List<GistContent>>

    fun getGist(id: String): Maybe<GistContent>

    fun getAvatar(author: GistAuthor?): Maybe<Bitmap>

    fun getImageFile(fileUrl: String): Maybe<Bitmap>

    fun getTextFile(fileUrl: String): Maybe<String>

    fun getFile(fileUrl: String): Maybe<ByteArray>
}
