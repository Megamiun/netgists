package br.com.gabryel.netgists.data.source

import br.com.gabryel.netgists.data.GistAuthor
import br.com.gabryel.netgists.data.GistContent
import br.com.gabryel.netgists.data.GistFile

/**
 * Repository for retrieval and manipulation of Gists
 *
 * Created by gabryel on 21/01/18.
 */
interface GistRepository: GistDataSource {
    fun saveGist(gist: GistContent)

    fun saveFileContent(gistFile: GistFile, content: ByteArray)

    fun saveAvatar(author: GistAuthor, content: ByteArray)
}