package br.com.gabryel.netgists.data.source.local

import io.requery.*

@Entity
interface Gist : Persistable {
    @get:Key
    var id: String

    @get:OneToOne(cascade = arrayOf(CascadeAction.NONE))
    @get:ForeignKey
    var author: Author?

    @get:OneToMany(mappedBy = "gist")
    var files: Set<File>
}

@Entity
interface File : Persistable {
    @get:Key
    @get:Generated
    var id: Int

    var filename: String
    var fileType: String
    var language: String
    var rawUrl: String

    @get:ManyToOne(cascade = arrayOf(CascadeAction.NONE))
    var gist: Gist
}

@Entity
interface Author : Persistable {
    @get:Key
    var id: Int

    var login: String
    var avatarUrl: String
}

@Entity
interface AuthorAvatar : Persistable {
    @get:Key
    @get:OneToOne
    @get:ForeignKey
    var author: Author

    var content: ByteArray
}

@Entity
interface FileContent : Persistable {
    @get:Key
    var rawUrl: String

    var content: ByteArray
}