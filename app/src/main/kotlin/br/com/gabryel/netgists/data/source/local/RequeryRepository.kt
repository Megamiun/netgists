package br.com.gabryel.netgists.data.source.local

import android.graphics.Bitmap
import br.com.gabryel.netgists.data.GistAuthor
import br.com.gabryel.netgists.data.GistContent
import br.com.gabryel.netgists.data.GistFile
import br.com.gabryel.netgists.data.source.GistRepository
import br.com.gabryel.netgists.ext.asBitmap
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.schedulers.Schedulers
import io.requery.Persistable
import io.requery.reactivex.KotlinReactiveEntityStore
import java.nio.charset.Charset

/**
 * Fetcher for GistContent information remotely from Github.
 *
 * Created by gabryel on 21/01/18.
 */
class RequeryRepository(private val entityStore: KotlinReactiveEntityStore<Persistable>)
    : GistRepository {

    override fun getGists(pageNo: Int, pageSize: Int): Flowable<List<GistContent>> {
        return entityStore
            .select(Gist::class)
            .limit(pageSize)
            .offset(pageNo * pageSize)
            .get().flowable()
            .map { it.toGistContent() }.toList().toFlowable()
    }

    override fun getGist(id: String): Maybe<GistContent> {
        return entityStore
            .select(Gist::class)
            .where(GistEntity.ID.eq(id))
            .limit(1)
            .get().maybe()
            .map { it.toGistContent() }
    }

    override fun getAvatar(author: GistAuthor?): Maybe<Bitmap> {
        author?: return Maybe.empty()

        return entityStore
            .select(AuthorAvatar::class)
            .where(AuthorAvatarEntity.AUTHOR_ID.eq(author.id))
            .limit(1)
            .get().maybe()
            .map { it.content.asBitmap() }
    }

    override fun getFile(fileUrl: String): Maybe<ByteArray> {
        return entityStore
            .select(FileContentEntity::class)
            .where(FileEntity.RAW_URL.eq(fileUrl))
            .limit(1)
            .get().maybe()
            .map { it.content }
    }

    override fun getImageFile(fileUrl: String) = getFile(fileUrl).map { it.asBitmap() }

    override fun getTextFile(fileUrl: String) =
        getFile(fileUrl).map { String(it, Charset.forName("UTF-8")) }

    override fun saveGist(gist: GistContent) {
        val entity = gist.toGist()

        entityStore.withTransaction {
            val oldGist = select(Gist::class)
                .where(GistEntity.ID.eq(entity.id))
                .get().firstOrNull()

            if (oldGist != null) return@withTransaction

            entity.author?.let {
                val oldAuthor = select(Author::class)
                    .where(AuthorEntity.ID.eq(it.id))
                    .get().firstOrNull()

                if (oldAuthor == null) insert(it)
            }

            insert(entity)
            entity.files.forEach { insert(it) }
        }.subscribeOn(Schedulers.io()).subscribe()
    }

    override fun saveFileContent(gistFile: GistFile, content: ByteArray) {
        entityStore.withTransaction {
            val old = select(FileContent::class)
                .where(FileContentEntity.RAW_URL.eq(gistFile.rawUrl))
                .get().firstOrNull()

            if (old != null) {
                old.content = content
                update(old)
            } else {
                val newItem = FileContentEntity()
                newItem.rawUrl = gistFile.rawUrl
                newItem.content = content
                insert(newItem)
            }
        }
    }

    override fun saveAvatar(author: GistAuthor, content: ByteArray) {
        entityStore.withTransaction {
            val old = select(AuthorAvatar::class)
                .where(AuthorAvatarEntity.AUTHOR_ID.eq(author.id))
                .get().firstOrNull()

            if (old != null) {
                old.content = content
                update(old)
            } else {
                val newItem = AuthorAvatarEntity()
                newItem.author = author.toAuthor()
                newItem.content = content
                insert(newItem)
            }
        }
    }

    private fun GistContent.toGist(): GistEntity {
        val gist = GistEntity()
        gist.id = id
        gist.author = owner?.toAuthor()
        gist.files = files.map { it.toFile(gist) }.toSet()

        return gist
    }

    private fun GistAuthor.toAuthor(): AuthorEntity {
        val author = AuthorEntity()
        author.id = id
        author.login = login
        author.avatarUrl = avatarUrl

        return author
    }

    private fun GistFile.toFile(gist: Gist): File {
        val file = FileEntity()
        file.filename = filename
        file.fileType = fileType
        file.language = language
        file.rawUrl = rawUrl
        file.gist = gist

        return file
    }

    private fun Gist.toGistContent(): GistContent {
        return GistContent(id,
                files.map { GistFile(it.filename, it.language, it.rawUrl, it.fileType) },
                author?.let { GistAuthor(it.id, it.login, it.avatarUrl) })
    }
}