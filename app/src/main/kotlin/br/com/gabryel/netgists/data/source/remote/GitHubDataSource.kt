package br.com.gabryel.netgists.data.source.remote

import android.graphics.Bitmap
import br.com.gabryel.netgists.data.GistAuthor
import br.com.gabryel.netgists.data.GistContent
import br.com.gabryel.netgists.data.GistFile
import br.com.gabryel.netgists.data.source.GistDataSource
import br.com.gabryel.netgists.ext.asBitmap
import br.com.gabryel.netgists.service.GistFileResponse
import br.com.gabryel.netgists.service.GistResponse
import br.com.gabryel.netgists.service.GitHubGistService
import br.com.gabryel.netgists.service.OwnerResponse
import io.reactivex.Maybe
import okhttp3.OkHttpClient
import okhttp3.Request
import java.io.InterruptedIOException
import java.nio.charset.Charset

/**
 * Fetcher for GistContent information remotely from Github.
 *
 * Created by gabryel on 21/01/18.
 */
class GitHubDataSource(private val gistService: GitHubGistService): GistDataSource {

    override fun getGists(pageNo: Int, pageSize: Int) =
        gistService.getGistSummary(pageNo, pageSize)
            .map { list -> list.map { item -> item.toGistContent() } }

    override fun getGist(id: String) = gistService.getGist(id).map { it.toGistContent() }

    override fun getAvatar(author: GistAuthor?): Maybe<Bitmap> {
        val url = author?.avatarUrl?: return Maybe.empty()
        return getImageFile(url)
    }

    override fun getFile(fileUrl: String) =
        Maybe.create<ByteArray> { emitter ->
            val request = Request.Builder().url(fileUrl).build()
            try {
                val body = OkHttpClient().newCall(request).execute().body()
                body?.bytes()?.let { emitter.onSuccess(it) }
            } catch (ex: InterruptedIOException) {
                if (!emitter.isDisposed) emitter.onError(ex)
            }
        }

    override fun getImageFile(fileUrl: String) = getFile(fileUrl).map { it.asBitmap() }

    override fun getTextFile(fileUrl: String) =
        getFile(fileUrl).map { String(it, Charset.forName("UTF-8")) }

    private fun GistResponse.toGistContent() =
        GistContent(id = id,
                files = files.values.map { it.toGistFile() },
                owner = owner?.toAuthor())

    private fun OwnerResponse.toAuthor() =
        GistAuthor(id = id,
                login = login,
                avatarUrl = avatar_url)

    private fun GistFileResponse.toGistFile() =
        GistFile(filename = filename,
                language = language,
                rawUrl = raw_url,
                fileType = type)
}