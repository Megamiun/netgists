package br.com.gabryel.netgists.data.source.remote

import br.com.gabryel.netgists.data.GistAuthor
import br.com.gabryel.netgists.data.GistContent
import br.com.gabryel.netgists.data.GistFile
import br.com.gabryel.netgists.data.source.GistDataSource
import br.com.gabryel.netgists.data.source.GistRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MixedGistRepository(private val localGistRepository: GistRepository,
                          val remoteGistDataSource: GistDataSource): GistRepository {
    override fun saveGist(gist: GistContent) {
        localGistRepository.saveGist(gist)

        gist.owner?.let { owner ->
            remoteGistDataSource.getFile(owner.avatarUrl)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe { saveAvatar(owner, it) }
        }

        gist.files.forEach { file ->
            remoteGistDataSource.getFile(file.rawUrl)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe { saveFileContent(file, it) }
        }
    }

    override fun saveFileContent(gistFile: GistFile, content: ByteArray) =
        localGistRepository.saveFileContent(gistFile, content)

    override fun saveAvatar(author: GistAuthor, content: ByteArray) =
        localGistRepository.saveAvatar(author, content)

    override fun getGists(pageNo: Int, pageSize: Int) =
        remoteGistDataSource.getGists(pageNo, pageSize)

    override fun getGist(id: String) =
        localGistRepository.getGist(id)
            .switchIfEmpty(remoteGistDataSource.getGist(id))
            .onErrorResumeNext(remoteGistDataSource.getGist(id))

    override fun getAvatar(author: GistAuthor?) =
        localGistRepository.getAvatar(author)
            .switchIfEmpty(remoteGistDataSource.getAvatar(author))
            .onErrorResumeNext(remoteGistDataSource.getAvatar(author))

    override fun getImageFile(fileUrl: String) =
        localGistRepository.getImageFile(fileUrl)
            .switchIfEmpty(remoteGistDataSource.getImageFile(fileUrl))
            .onErrorResumeNext(remoteGistDataSource.getImageFile(fileUrl))

    override fun getTextFile(fileUrl: String) =
        localGistRepository.getTextFile(fileUrl)
            .switchIfEmpty(remoteGistDataSource.getTextFile(fileUrl))
            .onErrorResumeNext(remoteGistDataSource.getTextFile(fileUrl))

    override fun getFile(fileUrl: String) =
        localGistRepository.getFile(fileUrl)
            .switchIfEmpty(remoteGistDataSource.getFile(fileUrl))
            .onErrorResumeNext(remoteGistDataSource.getFile(fileUrl))
}