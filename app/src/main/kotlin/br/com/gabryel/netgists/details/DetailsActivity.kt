package br.com.gabryel.netgists.details

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import br.com.gabryel.netgists.R
import br.com.gabryel.netgists.data.GistFile
import br.com.gabryel.netgists.gist.DetailsFragment
import br.com.gabryel.netgists.gist.FileFragment

class DetailsActivity : AppCompatActivity(), DetailsFragment.FileSelectCallback {
    companion object {
        val ONLY_LOCAL = "ONLY_SAVED"

        val GIST_ID = "GIST_ID"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        if (savedInstanceState != null) return

        val onlyLocal = intent.getBooleanExtra(ONLY_LOCAL, true)
        val gistId = intent.getStringExtra(GIST_ID)

        supportFragmentManager.beginTransaction()
            .add(R.id.frame_detail, DetailsFragment.newInstance(gistId, onlyLocal))
            .commit()
    }

    override fun openFileSelected(file: GistFile) {
        val onlyLocal = intent.getBooleanExtra(ONLY_LOCAL, true)

        supportFragmentManager
            .beginTransaction()
            .replace(R.id.frame_detail, FileFragment.newInstance(file, onlyLocal))
            .commit()
    }
}