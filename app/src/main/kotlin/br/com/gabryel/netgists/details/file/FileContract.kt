package br.com.gabryel.netgists.details.file;

import android.graphics.Bitmap
import br.com.gabryel.netgists.mvp.base.BasePresenter
import br.com.gabryel.netgists.mvp.base.BaseView

class FileContract {
    interface View : BaseView<Presenter> {

        /**
         * Shows or hides the Loading Indicator
         *
         * @param loading State of loading
         */
        fun setLoadingIndicator(loading: Boolean)

        fun showTextual(text: String, language: String? = null)

        fun showImage(image: Bitmap)
    }

    interface Presenter : BasePresenter
}