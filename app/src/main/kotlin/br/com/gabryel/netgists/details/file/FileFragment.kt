package br.com.gabryel.netgists.gist

import android.graphics.Bitmap
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.gabryel.netgists.Injection
import br.com.gabryel.netgists.R
import br.com.gabryel.netgists.data.GistFile
import br.com.gabryel.netgists.details.file.FileContract
import com.github.salomonbrys.kodein.LazyKodein
import com.github.salomonbrys.kodein.android.appKodein
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.with
import com.jsibbold.zoomage.ZoomageView
import io.github.kbiakov.codeview.CodeView
import kotlinx.android.synthetic.main.fragment_file.*

class FileFragment : Fragment(), FileContract.View {

    override lateinit var presenter: FileContract.Presenter

    private val kodein = LazyKodein(appKodein)

    override var myView: View? = null
        get() = view

    companion object {
        val FILE_URL = "FILE_URL"

        val FILE_TYPE = "FILE_TYPE"

        val ONLY_LOCAL = "ONLY_LOCAL"

        fun newInstance(file: GistFile, onlyLocal: Boolean) =
            FileFragment().apply {
                arguments = Bundle().apply {
                    putString(FILE_URL, file.rawUrl)
                    putString(FILE_TYPE, file.fileType)
                    putBoolean(ONLY_LOCAL, onlyLocal)
                }
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val fileUrl = arguments.getString(FILE_URL)
        val fileType = arguments.getString(FILE_TYPE)
        val onlyLocal = arguments.getBoolean(ONLY_LOCAL)
        val tag = if (onlyLocal) Injection.InjectionType.LOCAL else Injection.InjectionType.PRIMARILY_REMOTE

        presenter = kodein().with(fileType to fileUrl to this).instance(tag)
    }

    override fun onResume() {
        super.onResume()
        presenter.subscribe()
    }

    override fun onPause() {
        super.onPause()
        presenter.unsubscribe()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_file, container, false)
    }

    override fun setLoadingIndicator(loading: Boolean) {
        file_content_indeterminate_bar.visibility = if (loading) View.VISIBLE else View.GONE
    }

    override fun showTextual(text: String, language: String?) {
        val codeView = layoutInflater.inflate(R.layout.code_view, null) as CodeView
        file_content.addView(codeView,0)

        if (language != null) {
            codeView.setCode(text, language)
        } else {
            codeView.setCode(text)
        }
    }

    override fun showImage(image: Bitmap) {
        file_content.addView(ZoomageView(context).apply { this.setImageBitmap(image)},0)
    }
}
