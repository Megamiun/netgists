package br.com.gabryel.netgists.details.file

import br.com.gabryel.netgists.data.source.GistRepository
import br.com.gabryel.netgists.ext.setupCall
import br.com.gabryel.netgists.ext.warnAboutError
import io.reactivex.disposables.CompositeDisposable

class FilePresenter(private val gistDS: GistRepository,
                    private val fileType: String,
                    private val fileUrl: String,
                    override val view: FileContract.View): FileContract.Presenter {

    private val disposable = CompositeDisposable()

    override fun subscribe() {
        when {
            fileType.contains("image") ->
                gistDS.getImageFile(fileUrl).setupCall(view::setLoadingIndicator)
                    .subscribe(view::showImage, view::warnAboutError)

            // Will say that anything that is not a image, is text
            else ->
                gistDS.getTextFile(fileUrl).setupCall(view::setLoadingIndicator)
                    .subscribe( { view.showTextual(it, fileType) } , view::warnAboutError)
        }
    }

    override fun unsubscribe() { disposable.clear() }
}