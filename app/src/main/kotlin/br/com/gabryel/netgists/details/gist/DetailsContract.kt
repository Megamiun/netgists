package br.com.gabryel.netgists.gist

import android.graphics.Bitmap
import br.com.gabryel.netgists.data.GistFile
import br.com.gabryel.netgists.mvp.base.BaseListPresenter
import br.com.gabryel.netgists.mvp.base.BasePresenter
import br.com.gabryel.netgists.mvp.base.BaseRowView
import br.com.gabryel.netgists.mvp.base.BaseView

/**
 * Contract specifying the links between the Details View and Presenter
 *
 * Created by gabryel on 20/01/18.
 */
class DetailsContract {
    interface View : BaseView<Presenter> {

        /**
         * Show the name of the author on screen
         *
         * @param authorName Name of the author
         */
        fun showAuthor(authorName: String?)

        /**
         * Show the photo of the author on screen
         *
         * @param avatar Bitmap of user's avatar
         */
        fun showUserAvatar(avatar: Bitmap)

        /**
         * Shows or hides the Loading Indicator
         *
         * @param loading State of loading
         */
        fun setLoadingIndicator(loading: Boolean)

        /**
         * Change the screen to GistFile raw contents
         *
         * @param gistFile Id of the wanted GistContent to load
         */
        fun showGistRawFileUi(gistFile: GistFile)

        fun setListPresenter(presenter: ListPresenter)

        fun setFABOnClickListener(listener: (android.view.View) -> Unit)
    }

    interface Presenter : BasePresenter {
        /**
         * Change the screen to GistFile raw content
         *
         * @param gistFile File to load
         */
        fun openFile(gistFile: GistFile)
    }

    interface ListPresenter : BaseListPresenter {

        fun replaceData(files: List<GistFile>)

        fun bind(position: Int, row: FileRowView)

        fun getRowCount(): Int
    }

    interface FileRowView : BaseRowView<ListPresenter> {

        fun setFilename(filename: String)

        fun setLanguage(language: String?)

        fun setOnClickListener(listener: (android.view.View) -> Unit)
    }
}