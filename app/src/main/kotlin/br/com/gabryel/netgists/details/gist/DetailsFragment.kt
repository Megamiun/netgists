package br.com.gabryel.netgists.gist

import android.content.Context
import android.graphics.Bitmap
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.gabryel.netgists.Injection.InjectionType
import br.com.gabryel.netgists.R
import br.com.gabryel.netgists.data.GistFile
import br.com.gabryel.netgists.ext.warnUser
import com.github.salomonbrys.kodein.LazyKodein
import com.github.salomonbrys.kodein.android.appKodein
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.with
import kotlinx.android.synthetic.main.file_item.view.*
import kotlinx.android.synthetic.main.fragment_details.*

class DetailsFragment : Fragment(), DetailsContract.View {

    companion object {
        val ONLY_LOCAL = "ONLY_SAVED"

        val GIST_ID = "GIST_ID"

        @JvmStatic
        fun newInstance(gistId: String, onlyLocal: Boolean) =
            DetailsFragment().apply {
                arguments = Bundle().apply {
                    putBoolean(ONLY_LOCAL, onlyLocal)
                    putString(GIST_ID, gistId)
                }
            }
    }

    interface FileSelectCallback {
        fun openFileSelected(file: GistFile)
    }

    override var myView: View? = null
        get() = view

    override lateinit var presenter: DetailsContract.Presenter

    private lateinit var callBack: FileSelectCallback

    private val kodein = LazyKodein(appKodein)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val gistId = arguments.getString(GIST_ID)
        val onlyLocal = arguments.getBoolean(ONLY_LOCAL)
        val tag = if (onlyLocal) InjectionType.LOCAL else InjectionType.PRIMARILY_REMOTE

        presenter = kodein().with { gistId to this }.instance(tag)
    }

    override fun onResume() {
        super.onResume()
        presenter.subscribe()
    }

    override fun onPause() {
        super.onPause()
        presenter.unsubscribe()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context !is FileSelectCallback)
            throw IllegalStateException("${context::class.simpleName} must implement ${FileSelectCallback::class.simpleName}")

        callBack = context
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_details, container, false)
    }

    override fun showAuthor(authorName: String?) {
        gist_detail_author_name.text = authorName?: getString(R.string.anonymous_user)
    }

    override fun showUserAvatar(avatar: Bitmap) {
        gist_detail_avatar_image.setImageBitmap(avatar)
    }

    override fun setLoadingIndicator(loading: Boolean) {
        gist_details_indeterminate_bar.visibility = if (loading) View.VISIBLE else View.GONE
    }

    override fun showGistRawFileUi(gistFile: GistFile) {
        callBack.openFileSelected(gistFile)
    }

    override fun setListPresenter(presenter: DetailsContract.ListPresenter) {
        gist_detail_file_list.layoutManager = LinearLayoutManager(context)
        gist_detail_file_list.adapter = FileAdapter(presenter)

        presenter.adapter = gist_detail_file_list.adapter
    }

    override fun setFABOnClickListener(listener: (View) -> Unit) {
        save_button.setOnClickListener { view ->
            listener(view)
            warnUser(getString(R.string.message_gist_saved_with_success))
        }
    }

    private class FileAdapter(var presenter: DetailsContract.ListPresenter) :
            RecyclerView.Adapter<FileAdapter.FileHolder>() {

        override fun onBindViewHolder(holder: FileHolder, position: Int) {
            presenter.bind(position, holder)
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FileHolder {
            val rowView = LayoutInflater.from(parent.context)
                .inflate(R.layout.file_item, parent, false)

            return FileHolder(rowView)
        }

        override fun getItemCount() = presenter.getRowCount()

        class FileHolder(override var myView: View?): RecyclerView.ViewHolder(myView), DetailsContract.FileRowView {

            override fun setFilename(filename: String) { itemView.file_title.text = filename }

            override fun setLanguage(language: String?) { itemView.file_language.text = language }

            override fun setOnClickListener(listener: (View) -> Unit) { itemView.setOnClickListener(listener) }
        }
    }
}
