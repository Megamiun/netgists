package br.com.gabryel.netgists.gist

import android.support.v7.widget.RecyclerView
import br.com.gabryel.netgists.data.GistContent
import br.com.gabryel.netgists.data.GistFile
import br.com.gabryel.netgists.data.source.GistRepository
import br.com.gabryel.netgists.ext.setupCall
import br.com.gabryel.netgists.ext.warnAboutError
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class DetailsPresenter(private val gistDS: GistRepository,
                       private val gistId: String,
                       override val view: DetailsContract.View): DetailsContract.Presenter {

    private val disposable = CompositeDisposable()

    private lateinit var listPresenter: DetailsContract.ListPresenter

    override fun openFile(gistFile: GistFile) { view.showGistRawFileUi(gistFile) }

    override fun subscribe() {
        listPresenter = FileRowPresenter()
        view.setListPresenter(listPresenter)

        val subscription = gistDS.getGist(gistId)
            .setupCall(view::setLoadingIndicator)
            .subscribe(this::updateGistInfos, view::warnAboutError)

        disposable.add(subscription)
    }

    override fun unsubscribe() { disposable.clear() }

    private fun updateGistInfos(gist: GistContent) {
        view.showAuthor(gist.owner?.login)
        listPresenter.replaceData(gist.files)
        view.setFABOnClickListener {
            gistDS.saveGist(gist)
        }

        val subscription = gistDS.getAvatar(gist.owner)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(view::showUserAvatar, view::warnAboutError)

        disposable.add(subscription)
    }

    inner class FileRowPresenter(private var files: List<GistFile> = emptyList())
        : DetailsContract.ListPresenter {

        override lateinit var adapter: RecyclerView.Adapter<*>

        override fun replaceData(files: List<GistFile>) {
            this.files = files
            adapter.notifyDataSetChanged()
        }

        override fun bind(position: Int, row: DetailsContract.FileRowView) {
            val file = files[position]

            row.setFilename(file.filename)
            row.setLanguage(file.language)
            row.setOnClickListener { _ -> openFile(file) }
        }

        override fun getRowCount() = files.size
    }
}