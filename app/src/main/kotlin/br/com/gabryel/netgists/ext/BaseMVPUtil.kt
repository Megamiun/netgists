package br.com.gabryel.netgists.ext

import android.graphics.BitmapFactory
import android.support.design.widget.Snackbar
import android.util.Log
import br.com.gabryel.netgists.R
import br.com.gabryel.netgists.mvp.base.Viewable
import io.reactivex.Maybe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException

fun <T> Maybe<T>.setupCall(setLoadingProgressBar: (Boolean) -> Unit): Maybe<T> {
    return observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io())
        .doOnSubscribe { setLoadingProgressBar(true) }
        .doFinally { setLoadingProgressBar(false) }
}

fun ByteArray.asBitmap() = BitmapFactory.decodeByteArray(this, 0, size)

/**
 * Logs the error message and warns the user about it.
 */
fun Viewable.warnAboutError(ex: Throwable) {
    Log.e(this::class.simpleName, ex.message)
    if (ex !is HttpException) {
        showGeneralError()
        return
    }

    showNetworkError(ex.code())
}

/**
 * Warn user about an error
 *
 * @param errorCode Code of the given error
 */
fun Viewable.showNetworkError(errorCode: Int) =
    myView?.let { warnUser(it.context.getString(R.string.message_network_error, errorCode)) }

/**
 * Warn user about an generic error
 */
fun Viewable.showGeneralError() =
    myView?.let { warnUser(it.context.getString(R.string.message_unexpected_error))}

fun Viewable.warnUser(message: String) =
    myView?.let { Snackbar.make(it, message, Snackbar.LENGTH_LONG).show() }