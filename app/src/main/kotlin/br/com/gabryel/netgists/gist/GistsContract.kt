package br.com.gabryel.netgists.gist

import android.graphics.Bitmap
import br.com.gabryel.netgists.data.GistContent
import br.com.gabryel.netgists.mvp.base.BaseListPresenter
import br.com.gabryel.netgists.mvp.base.BasePresenter
import br.com.gabryel.netgists.mvp.base.BaseRowView
import br.com.gabryel.netgists.mvp.base.BaseView


/**
 * Contract specifying the links between the Home View and Presenter
 *
 * Created by gabryel on 20/01/18.
 */
class GistsContract {
    interface View : BaseView<Presenter> {

        /**
         * Shows or hides the Loading Indicator
         *
         * @param loading State of loading
         */
        fun setLoadingIndicator(loading: Boolean)

        /**
         * Change the screen to GistContent Details
         *
         * @param gistId Id of the wanted GistContent to load
         */
        fun showGistDetailsUi(gistId: String)

        /**
         * Set a list presenter on the View
         */
        fun setListPresenter(presenter: ListPresenter)
    }

    interface Presenter : BasePresenter {
        /**
         * Change the screen to GistContent Details
         *
         * @param gist GistContent to load
         */
        fun openGist(gist: GistContent)

        /**
         * Loads a given page of Gists
         *
         * @param pageNo number of page to be loaded
         * @param pageSize size of a page
         */
        fun loadPage(pageNo: Int, pageSize: Int = 30)
    }

    interface ListPresenter : BaseListPresenter {
        fun bind(position: Int, holder: GistsRowView)

        fun reset(holder: GistsRowView)

        fun replaceData(gists: List<GistContent>)

        fun getRowCount(): Int
    }

    interface GistsRowView : BaseRowView<ListPresenter> {
        fun setLanguage(language: String?)

        fun setAuthor(authorName: String?)

        fun setTitle(tile: String)

        fun setAvatar(avatar: Bitmap)

        fun resetAvatar()

        fun setOnClickListener(listener: (android.view.View) -> Unit)
    }
}