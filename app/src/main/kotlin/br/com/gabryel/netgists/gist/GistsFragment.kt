package br.com.gabryel.netgists.gist

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.gabryel.netgists.Injection.InjectionType
import br.com.gabryel.netgists.R
import br.com.gabryel.netgists.details.DetailsActivity
import com.github.salomonbrys.kodein.LazyKodein
import com.github.salomonbrys.kodein.android.appKodein
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.with
import kotlinx.android.synthetic.main.gist_item.view.*
import kotlinx.android.synthetic.main.loadable_list.*

class GistsFragment : Fragment(), GistsContract.View {

    companion object {
        val ONLY_LOCAL = "ONLY_SAVED"

        @JvmStatic
        fun newInstance(onlyLocal: Boolean) =
            GistsFragment().apply {
                arguments = Bundle().apply { putBoolean("ONLY_SAVED", onlyLocal) }
            }
    }

    override var myView: View? = null
        get() = view

    override lateinit var presenter: GistsContract.Presenter

    private val kodein = LazyKodein(appKodein)

    private lateinit var userDefaultAvatar: Drawable

    override fun onCreate(savedInstanceState: Bundle?) {
        userDefaultAvatar = ContextCompat.
            getDrawable(context, R.drawable.ic_account_circle_transparent_24px)

        super.onCreate(savedInstanceState)

        val onlyLocal = arguments.getBoolean(ONLY_LOCAL)
        val tag = if (onlyLocal) InjectionType.LOCAL else InjectionType.PRIMARILY_REMOTE

        presenter = kodein().with(this).instance(tag)
    }

    override fun onResume() {
        super.onResume()
        presenter.subscribe()
    }

    override fun onPause() {
        super.onPause()
        presenter.unsubscribe()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.loadable_list, container, false)
    }

    override fun setListPresenter(presenter: GistsContract.ListPresenter) {
        list_recycler.layoutManager = LinearLayoutManager(context)
        list_recycler.adapter = GistAdapter(presenter)

        presenter.adapter = list_recycler.adapter
    }

    override fun setLoadingIndicator(loading: Boolean) {
        indeterminate_bar.visibility = if (loading) View.VISIBLE else View.GONE
    }

    override fun showGistDetailsUi(gistId: String) {
        val intent = Intent(context, DetailsActivity::class.java)
        intent.putExtra(DetailsActivity.GIST_ID, gistId)
        intent.putExtra(DetailsActivity.ONLY_LOCAL, arguments.getBoolean(ONLY_LOCAL))
        startActivity(intent)
    }

    private inner class GistAdapter(private val presenter: GistsContract.ListPresenter) :
            RecyclerView.Adapter<GistAdapter.GistHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GistHolder {
            val rowView = LayoutInflater.from(parent.context)
                .inflate(R.layout.gist_item, parent, false)

            return GistHolder(rowView)
        }

        override fun onBindViewHolder(holder: GistHolder, position: Int) {
            presenter.bind(position, holder)
        }

        override fun onViewRecycled(holder: GistHolder) {
            presenter.reset(holder)
        }

        override fun getItemId(position: Int) = position.toLong()

        override fun getItemCount() = presenter.getRowCount()

        inner class GistHolder(override var myView: View?): RecyclerView.ViewHolder(myView), GistsContract.GistsRowView {

            override fun setLanguage(language: String?) { itemView.gist_language.text = language }

            override fun setAuthor(authorName: String?) {
                itemView.gist_author.text = authorName?:
                        this@GistsFragment.getString(R.string.anonymous_user)
            }

            override fun setAvatar(avatar: Bitmap) {
                itemView.gist_avatar_image.setImageBitmap(avatar)
            }

            override fun resetAvatar() {
                itemView.gist_avatar_image.setImageDrawable(userDefaultAvatar)
            }

            override fun setTitle(tile: String) { itemView.gist_title.text = tile }

            override fun setOnClickListener(listener: (View) -> Unit) { itemView.setOnClickListener(listener) }
        }
    }
}
