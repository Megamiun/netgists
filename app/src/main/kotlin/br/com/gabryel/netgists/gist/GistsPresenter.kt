package br.com.gabryel.netgists.gist

import android.support.v7.widget.RecyclerView
import br.com.gabryel.netgists.data.GistContent
import br.com.gabryel.netgists.data.source.GistRepository
import br.com.gabryel.netgists.ext.warnAboutError
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class GistsPresenter(private val gistDS: GistRepository,
                     override val view: GistsContract.View): GistsContract.Presenter {

    private val presenter = GistRowPresenter()

    private val disposable = CompositeDisposable()

    override fun loadPage(pageNo: Int, pageSize: Int) {
        val subscription = gistDS.getGists(pageNo, pageSize)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { view.setLoadingIndicator(true) }
            .doOnTerminate { view.setLoadingIndicator(false) }
            .subscribe(presenter::replaceData, view::warnAboutError)

        disposable.add(subscription)
    }

    override fun subscribe() {
        view.setListPresenter(presenter)
        loadPage(0)
    }

    override fun unsubscribe() { disposable.clear() }

    override fun openGist(gist: GistContent) { view.showGistDetailsUi(gist.id) }

    inner class GistRowPresenter(private var gists: List<GistContent> = emptyList())
        : GistsContract.ListPresenter {

        override lateinit var adapter: RecyclerView.Adapter<*>

        val disposableMap = mutableMapOf<GistsContract.GistsRowView, Disposable>()

        override fun replaceData(gists: List<GistContent>) {
            this.gists = gists
            adapter.notifyDataSetChanged()
        }

        override fun reset(holder: GistsContract.GistsRowView) {
            val disposable = disposableMap[holder]
            if (disposable?.isDisposed == false) disposable.dispose()

            holder.resetAvatar()
        }

        override fun bind(position: Int, holder: GistsContract.GistsRowView) {
            val gist = gists[position]

            val fileCounter = if (gist.files.size == 1) "" else "(+${gist.files.size - 1} files)"

            holder.setAuthor(gist.owner?.login)
            holder.setTitle("${gist.files.first().filename}$fileCounter")
            holder.setLanguage(gist.files.first().language)
            holder.setOnClickListener { openGist(gist) }

            val avatarSub = gistDS.getAvatar(gist.owner)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(holder::setAvatar, view::warnAboutError)

            disposableMap.put(holder, avatarSub)
        }

        override fun getRowCount() = gists.size
    }
}