package br.com.gabryel.netgists.mvp.base

import android.support.v7.widget.RecyclerView

/**
 * Based on Android Architecture Blueprints for MVP (https://github.com/googlesamples/android-architecture/tree/todo-mvp/todoapp),
 * it represents the base definition of a Presenter for a MVP contract
 *
 * Created by gabryel on 20/01/18.
 */
interface BaseListPresenter {
    var adapter: RecyclerView.Adapter<*>
}