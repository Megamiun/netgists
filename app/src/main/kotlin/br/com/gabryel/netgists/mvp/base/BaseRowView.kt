package br.com.gabryel.netgists.mvp.base

/**
 * Based on Android Architecture Blueprints for MVP (https://github.com/googlesamples/android-architecture/tree/todo-mvp/todoapp),
 * it represents the base definition of a View for a MVP contract
 *
 * Created by gabryel on 20/01/18.
 */
interface BaseRowView<T: BaseListPresenter>: Viewable