package br.com.gabryel.netgists.mvp.base

import android.view.View

/**
 * Created by gabryel on 24/01/18.
 */
interface Viewable {
    var myView: View?
}