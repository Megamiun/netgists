package br.com.gabryel.netgists.service

data class GistResponse(val id: String,
                        val owner: OwnerResponse?,
                        val files: Map<String, GistFileResponse>)

data class GistFileResponse(val filename: String,
                            val language: String?,
                            val raw_url: String,
                            val type: String)

data class OwnerResponse(val id: Int,
                         val login: String,
                         val avatar_url: String)