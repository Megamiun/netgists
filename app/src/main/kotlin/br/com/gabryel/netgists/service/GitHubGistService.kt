package br.com.gabryel.netgists.service

import io.reactivex.Flowable
import io.reactivex.Maybe
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Service Declaration to fetch Gists from Github
 *
 * Created by gabryel on 21/01/18.
 */
interface GitHubGistService {
    @GET("/gists/public")
    fun getGistSummary(@Query("page") page: Int,
                       @Query("per_page") pageSize: Int): Flowable<List<GistResponse>>

    @GET("/gists/{id}")
    fun getGist(@Path("id") id: String): Maybe<GistResponse>
}