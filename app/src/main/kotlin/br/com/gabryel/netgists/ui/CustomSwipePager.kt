package br.com.gabryel.netgists.ui

import android.content.Context
import android.support.v4.view.ViewPager
import android.util.AttributeSet
import android.view.MotionEvent
import br.com.gabryel.netgists.R

/**
 * Copied from https://android.jlelse.eu/ultimate-guide-to-bottom-navigation-on-android-75e4efb8105f
 * transformed to Kotlin and added the possibility of configuring it on the xml
 */
class CustomSwipePager(context: Context, attrs: AttributeSet) : ViewPager(context, attrs) {

    private val pagingEnabled: Boolean

    init {
        val attributes = context.theme.obtainStyledAttributes(attrs,
                R.styleable.CustomSwipePager,0, 0)
        try {
            pagingEnabled =
                    attributes.getBoolean(R.styleable.CustomSwipePager_allowSwiping, true)
        } finally {
            attributes.recycle()
        }
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        return if (pagingEnabled) {
            super.onTouchEvent(event)
        } else false
    }

    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
        return if (pagingEnabled) {
            super.onInterceptTouchEvent(event)
        } else false
    }
}